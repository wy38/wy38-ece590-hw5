Basic Functions:

1. Use NSKeyArchiver to persist all fields to disk, including picture.

2. Convert the pictures and store the pictures.

3. Save all the data model as preferred type.

4. Add "team" to "StudentInfo" class.

5. Add, delete and edit from the Table View screen. Use "+" on the top to add information. Swipe to left to edit current row or delete current row.

Extra:

1. Name Search of the table.

2. Store additional field types "Birthday".

3. More complex search - Support multiple search (name search and degree search). Also when search someone's information, you can also swipe to left to edit or delete this person's information.

4. Add placeholder in information edit page, hence making the input information's format be uniform.