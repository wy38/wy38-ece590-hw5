//
//  ThirdViewController.swift
//  Homework 5
//
//  Created by Yuan on 2/16/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit
import AVFoundation

// Function for fade in and out
extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

class ThirdViewController: UIViewController {

    @IBOutlet weak var myFavouritePic: UIImageView!
    
    var audioplayer = AVAudioPlayer()
    
    var blk = block()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add shadows
        self.myFavouritePic.layer.shadowColor = UIColor.black.cgColor
        self.myFavouritePic.layer.shadowOpacity = 1
        self.myFavouritePic.layer.shadowOffset = CGSize.zero
        self.myFavouritePic.layer.shadowRadius = 10
        
        // audio playing
        do {
            audioplayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource:"Starry_Mayday", ofType:"mp3")!))
            audioplayer.play()
        }
        catch {
            print(error)
        }

        let tapGR = UITapGestureRecognizer(target: self, action: #selector(ThirdViewController.didTap(_:)))
        
        self.view.addGestureRecognizer(tapGR)
        
        // black background
        blk.frame = CGRect(x: 0, y: 0, width: 320, height: 338)
        blk.backgroundColor = UIColor.black
        blk.isHidden = false
        self.view.addSubview(blk)
        
    }
    
    // tap anywhere to add stars
    func didTap(_ tapGR: UITapGestureRecognizer) {
        
        let tapPoint = tapGR.location(in: self.view)
        let shapeView = drawingView(origin: tapPoint)
        self.view.addSubview(shapeView)
        shapeView.fadeOut(completion: {
            (finished: Bool) -> Void in
            shapeView.fadeIn()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // load image
    override func viewDidAppear(_ animated: Bool) {
        myFavouritePic.image = UIImage(named:"StarryNight.jpg")
        // fade in and fade out animation
        self.myFavouritePic.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.myFavouritePic.fadeIn()
        })
    }
}
