//
//  FirstTableViewController.swift
//  Homework 5
//
//  Created by Yuan on 2/8/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

// global variable to store the information for student selected
var stuInfo_global = StudentInfo(name : "", gender : true, hometown : "", degree : "", team : "", birth : "", image : #imageLiteral(resourceName: "default.jpg") )

var isNew : Bool = true

var row : Int = 0

var filterrow : Int = 0

class FirstTableViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate {

    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    
    var filtered = [StudentInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadInitialData()
        self.searchBar.delegate = self
        self.searchBar.scopeButtonTitles = ["Name", "Degree"]
        self.searchBar.selectedScopeButtonIndex = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (self.searchBar.text != "") {
            searchActive = true
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch(section) {
            case 0:
                return CGFloat.leastNormalMagnitude
            default:
                return CGFloat.leastNormalMagnitude
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch(section) {
            case 0:
                return CGFloat.leastNormalMagnitude
            default:
                return CGFloat.leastNormalMagnitude
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if (searchActive) {
            return 1
        } else {
            return 2
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchActive) {
            return filtered.count
        }
        switch (section) {
            case 0:
                return 1
            default:
                return self.studentLists.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (searchActive) {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentInfoCell", for: indexPath)
            let tempStuInfo : StudentInfo = self.filtered[indexPath.row]
            cell.imageView?.image = tempStuInfo.image
            cell.textLabel?.text = tempStuInfo.name
            cell.detailTextLabel?.text = tempStuInfo.degree
            return cell
        } else {
            switch(indexPath.section) {
                case 0:
                    let cell = Bundle.main.loadNibNamed("FirstTableViewCell", owner: self, options: nil)?.first as! FirstTableViewCell
                    cell.label1.text = teamLists
                    cell.image1.image = UIImage(named:"background")
                    return cell
            default:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentInfoCell", for: indexPath)
                let tempStuInfo : StudentInfo = self.studentLists[indexPath.row]
                cell.imageView?.image = tempStuInfo.image
                cell.textLabel?.text = tempStuInfo.name
                cell.detailTextLabel?.text = tempStuInfo.degree
                return cell
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        if (searchActive) {
            stuInfo_global = self.filtered[indexPath.row]
            row = self.findRow(original: self.studentLists, stu: stuInfo_global)
            filterrow = indexPath.row
        } else {
            stuInfo_global = self.studentLists[indexPath.row]
            row = indexPath.row
        }
        let _ = StudentInfo.saveStuInfo(studentLists)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
    }

    
    // Unwind Segue to Main
    @IBAction func unwindtoMain(Segue: UIStoryboardSegue) {
        
    }
    
    var teamLists : String = "WheW"
    
    var studentLists = [StudentInfo]()
    
    // Pre-populate the Table with “hardcoded” information
    func loadInitialData() {
        if let tempInfos = StudentInfo.loadStuInfo() {
            studentLists = tempInfos
        } else {
            let stu1 = StudentInfo(name : "Wanxin Yuan", gender : false,
                                   hometown : "Nanjing, Jiangsu, China",  degree : "Master", team : "Whew", birth : "October 3", image : #imageLiteral(resourceName: "Wanxin.png"))
            stu1.interest = convertStringtoArray(word: "listening to music,watching movies,travelling")
            stu1.programming = convertStringtoArray(word: "C,C++,Python")
            studentLists.append(stu1)
            
            let stu2 = StudentInfo(name : "Wuyi Sun", gender : false,
                                   hometown : "Zhenjiang, Jiangsu, China",  degree : "Master", team : "Whew", birth : "March 19", image : #imageLiteral(resourceName: "Wuyi.png"))
            stu2.interest = convertStringtoArray(word: "watching movies,running")
            stu2.programming = convertStringtoArray(word: "C,C++,Javascript,HTML")
            studentLists.append(stu2)
            
            let stu3 = StudentInfo(name : "Yuhan Liu", gender : false,
                                   hometown : "Xi'an, Shanxi, China",  degree : "Master", team : "Whew", birth : "August 28", image : #imageLiteral(resourceName: "Yuhan.png"))
            stu3.interest = convertStringtoArray(word: "watching movies,running")
            stu3.programming = convertStringtoArray(word: "Java,C++,C,C#")
            studentLists.append(stu3)
            
            let _ = StudentInfo.saveStuInfo(studentLists)
        }
    }
    
    
    // Add or modify a new student's information on the table
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
        let source:ViewController = segue.source as! ViewController
        let stu:StudentInfo = source.stuInfo
        if (stu.name != "") {
            if (isNew) {
                self.studentLists.append(stu)
            } else {
                self.studentLists[row] = stu
                if (self.searchBar.text != "") {
                    searchActive = true
                    self.filtered[filterrow] = stu
                }
            }
        }
        isNew = true
        let _ = StudentInfo.saveStuInfo(studentLists)
        self.tableView.reloadData()
    }
    
    // Function to delete rows or edit from table view
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        // choose to delete current row
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // remove the item from the data model
            if (self.searchActive) {
                self.studentLists.remove(at: self.findRow(original: self.studentLists, stu: self.filtered[indexPath.row]))
                self.filtered.remove(at: indexPath.row)
            } else {
                self.studentLists.remove(at: indexPath.row)
            }
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            let _ = StudentInfo.saveStuInfo(self.studentLists)
            self.tableView.reloadData()

        }
        
        // choose to edit current row
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            if (self.searchActive) {
                stuInfo_global = self.filtered[indexPath.row]
                row = self.findRow(original: self.studentLists, stu: stuInfo_global)
                filterrow = indexPath.row
            } else {
                stuInfo_global = self.studentLists[indexPath.row]
                row = indexPath.row
            }
            isNew = false
            self.performSegue(withIdentifier: "editInfoFromMainSegue", sender: self)
        }
        delete.backgroundColor = UIColor.red
        edit.backgroundColor = UIColor.blue
        
        return [delete, edit]
    }
    
    func findRow(original : [StudentInfo], stu : StudentInfo) -> Int {
        for i in 0 ..< original.count {
            if (original[i].name == stu.name) {
                return i
            }
        }
        return -1
    }
    
    // search
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.tableView.reloadData()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        self.tableView.reloadData()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        switch searchBar.selectedScopeButtonIndex {
        case 0:
            filtered = studentLists.filter({ (student) -> Bool in
                return student.name.lowercased().contains(searchText.lowercased())
            })
            self.tableView.reloadData()
        case 1:
            filtered = studentLists.filter({ (student) -> Bool in
                return student.degree.lowercased().contains(searchText.lowercased())
            })
            self.tableView.reloadData()
        default:
            print("no types")
        }
    }

}
