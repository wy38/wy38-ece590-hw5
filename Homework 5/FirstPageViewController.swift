//
//  FirstPageViewController.swift
//  Homework 5
//
//  Created by Yuan on 2/16/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class FirstPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    lazy var vcArr: [UIViewController] = {
        return [self.VCInstance(name: "FirstPersonInfoNavi"),
                self.VCInstance(name: "ThirdViewController")]
    }()
    
    private func VCInstance(name: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        if let firstVC = vcArr.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        if (stuInfo_global.name != "Wanxin Yuan") {
            return nil
        }
        guard let viewControllerIndex = vcArr.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return vcArr.last
        }
        
        guard vcArr.count > previousIndex else {
            return nil
        }
        
        return vcArr[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if (stuInfo_global.name != "Wanxin Yuan") {
            return nil
        }
        
        guard let viewControllerIndex = vcArr.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < vcArr.count else {
            return vcArr.first
        }
        
        guard vcArr.count > nextIndex else {
            return nil
        }
        
        return vcArr[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return vcArr.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = vcArr.index(of:firstViewController) else {
                return 0
        }
        return firstViewControllerIndex
    }
}
