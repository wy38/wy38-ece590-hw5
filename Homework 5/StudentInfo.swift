//
//  StudentInfo.swift
//  Homework 5
//
//  Created by Yuan on 2/8/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import Foundation
import UIKit

struct InfoKey {
    static let nameK = "Name"
    static let teamK = "Team"
    static let genderK = "Gender"
    static let hometownK = "Location"
    static let interestK = "Hobbies"
    static let degreeK = "Degree"
    static let programmingK = "Programming languages"
    static let birthK = "birthday"
    static let imageK = "Image"
}

func convertArraytoString(words: [String]) -> String {
    var res : String = ""
    if words.count == 1 {
        return words[0]
    }
    if words.count > 1 {
        for i in 0 ..< words.count - 1 {
            res += words[i] + ","
        }
        res += words[words.endIndex-1]
    }
    return res
}

func convertStringtoArray(word: String) -> [String] {
    var res = [String]()
    var temp : String = ""
    for i in 0 ..< word.characters.count {
        if (word[word.index(word.startIndex, offsetBy: i)] == ",") {
            res.append(temp)
            temp = ""
        } else {
            temp.append(word[word.index(word.startIndex, offsetBy: i)])
        }
    }
    if (temp != "") {
        res.append(temp)
    }
    return res
}

class PersonInfo : NSObject, NSCoding {
    //student's name
    var name : String = ""
    //student's gender
    var gender : Bool
    // student's hometown
    var hometown : String = ""
    // interests outside of school
    var interest = [String]()
    init(name : String, gender : Bool, hometown : String) {
        self.name = name
        self.gender = gender
        self.hometown = hometown
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: InfoKey.nameK)
        aCoder.encode(gender, forKey: InfoKey.genderK)
        aCoder.encode(hometown, forKey: InfoKey.hometownK)
        aCoder.encode(interest, forKey: InfoKey.interestK)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: InfoKey.nameK) as! String
        let gender = aDecoder.decodeBool(forKey: InfoKey.genderK)
        let hometown = aDecoder.decodeObject(forKey: InfoKey.hometownK) as! String
        let interest = aDecoder.decodeObject(forKey: InfoKey.interestK) as! [String]
        self.init(name : name , gender : gender, hometown : hometown)
        self.interest = interest
    }
}

class StudentInfo: PersonInfo {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("StudentInfoFile")
    
    // degree
    var degree : String = ""
    
    // proficient programming languages
    var programming = [String]()
    
    // team information
    var team : String = ""
    
    // birthday information
    var birth : String = ""
    
    // personal image
    var image : UIImage
    
    init(name : String, gender : Bool, hometown : String, degree : String, team : String, birth : String, image : UIImage) {
        self.degree = degree
        self.team = team
        self.image = image
        self.birth = birth
        super.init(name : name, gender : gender, hometown : hometown)
    }
    
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: InfoKey.nameK)
        aCoder.encode(gender, forKey: InfoKey.genderK)
        aCoder.encode(team, forKey: InfoKey.teamK)
        aCoder.encode(hometown, forKey: InfoKey.hometownK)
        aCoder.encode(degree, forKey: InfoKey.degreeK)
        aCoder.encode(interest, forKey: InfoKey.interestK)
        aCoder.encode(programming, forKey: InfoKey.programmingK)
        aCoder.encode(birth, forKey: InfoKey.birthK)
        let imageData = UIImageJPEGRepresentation(image, 0.9)
        aCoder.encode(imageData, forKey: InfoKey.imageK)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: InfoKey.nameK) as! String
        let team = aDecoder.decodeObject(forKey: InfoKey.teamK) as! String
        let gender = aDecoder.decodeBool(forKey: InfoKey.genderK)
        let hometown = aDecoder.decodeObject(forKey: InfoKey.hometownK) as! String
        let degree = aDecoder.decodeObject(forKey: InfoKey.degreeK) as! String
        let interest = aDecoder.decodeObject(forKey: InfoKey.interestK) as! [String]
        let programming = aDecoder.decodeObject(forKey: InfoKey.programmingK) as! [String]
        let birth = aDecoder.decodeObject(forKey: InfoKey.birthK) as! String
        let image = UIImage(data: aDecoder.decodeObject(forKey: InfoKey.imageK) as! Data)
        self.init(name : name , gender : gender, hometown : hometown, degree : degree, team : team, birth : birth, image : image!)
        self.interest = interest
        self.programming = programming
    }
    
    override var description : String {
        let temp_interest = convertArraytoString(words: self.interest)
        let temp_programming = convertArraytoString(words: self.programming)
        if (gender) {
            return "\(self.name) is from \(self.hometown). He is a member of team \(self.team). His birthday is \(self.birth). He is working on \(self.degree) degree. He is proficient in programming languages such as \(temp_programming). Additionally, he enjoys \(temp_interest) outside of school. "
        } else {
            return "\(self.name) is from \(self.hometown). She is a member of team \(self.team). Her birthday is \(self.birth). She is working on \(self.degree) degree. She is proficient in programming languages such as \(temp_programming). Additionally, she enjoys \(temp_interest) outside of school."
        }
    }
    
    static func saveStuInfo(_ stuInfo: [StudentInfo]) -> Bool {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(stuInfo, toFile: StudentInfo.ArchiveURL.path)
        if !isSuccessfulSave {
            print("Failed to save student's info")
            return false
        } else {
            return true
        }
    }
    
    static func loadStuInfo() -> [StudentInfo]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: StudentInfo.ArchiveURL.path) as? [StudentInfo]
    }



}
